import React from 'react';
import './App.css';
import Renderer from './components/Renderer';
import Update from './components/Update';
import { GlobalStateContext, store } from './Context';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.addTask = (payload) => {
      this.setState({
        tasks: [...this.state.tasks, payload] 
      });
    };

    this.removeTasks = () => {
      this.setState({
        tasks: []
      });
    };
    
    this.removeTask = (idx) => {
      this.setState({
        tasks: this.state.tasks.splice(idx, 1)
      });
    };

    this.state = {
      tasks: store.tasks,
      addTask: this.addTask,
      removeTask: this.removeTask,
      removeTasks: this.removeTasks
    };
  };

  render() {
    return (
      <GlobalStateContext.Provider value={this.state}>
        <center>
        <h2><u><i> Tasks </i></u></h2>
        <br />
        <Update />
        <Renderer />
        </center>
      </GlobalStateContext.Provider>
    );
  }
}

export default App;
