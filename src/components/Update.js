import React from 'react';
import { GlobalStateContext } from '../Context';
import { Button, InputGroup, FormControl } from 'react-bootstrap';

const Update = () => {
    const [task, setTask] = React.useState('');

    return (
        <GlobalStateContext.Consumer>
            {({tasks, addTask, removeTasks}) => (
                <>
                    <InputGroup className="mb-3" style={{width: '30vw'}}>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="basic-addon1">{tasks.length}</InputGroup.Text>
                        </InputGroup.Prepend>
                    
                        <FormControl
                        placeholder="Username"
                        aria-label="Username"
                        aria-describedby="basic-addon1"
                        onChange={(e) => setTask(e.target.value)}
                        required
                        />
                    </InputGroup>

                    <Button onClick={() => {
                        if (task !== '') {
                            addTask(task);
                            setTask('');
                        } else {
                          alert('Kindly provide a task');  
                        }
                    }}
                    >Add Task</Button>

                    <Button variant="danger" className="ml-5" onClick={() => removeTasks()}>
                        Remove Tasks
                    </Button>
                </>
            )}
        </GlobalStateContext.Consumer>
    );
};

export default Update;