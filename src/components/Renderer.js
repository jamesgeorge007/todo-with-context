import React from 'react';
import { GlobalStateContext } from '../Context';
import { Button, ListGroup } from 'react-bootstrap';

const Renderer = () => {
    return (
        <GlobalStateContext.Consumer>
            {({tasks, removeTask}) => (
            <ListGroup className="mt-5" variant="flush" style={{width: '30vw'}}>
                {tasks.map((task, idx) => 
                <ListGroup.Item key={idx}>{task} 
                <Button variant="danger" className='ml-5' onClick={removeTask(idx)}>Remove</Button>
                </ListGroup.Item>)}
            </ListGroup>
        
            )}
        </GlobalStateContext.Consumer>
    );
};

export default Renderer;