import React from 'react';

export const store = {
    tasks: [],
    addTask: () => {},
    removeTask: () => {},
    removeTasks: () => {}
};

export const GlobalStateContext = React.createContext(store);